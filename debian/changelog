3dchess (0.8.1-22) unstable; urgency=medium

  * Team upload.
  * Cherry-pick downstream Ubuntu patch to fix time64 transition:

  [ Steve Langasek ]
  * debian/patches/no-implicit-declarations.patch: fix missing
    declaration of time().  Closes: #1066305.

 -- Boyuan Yang <byang@debian.org>  Thu, 18 Apr 2024 21:30:57 -0400

3dchess (0.8.1-21) unstable; urgency=medium

  * Team upload.
  * Refresh packaging:
    + Bump Standards-Version to 4.6.0.
    + Bump debhelper compat to v13.
    + Update Vcs-* fields.
    + Use "Rules-Requires-Root: no".
  * debian/watch: Monitor upstream at ibiblio.org.
  * debian/control: Set homepage to ibiblio.org archive.

 -- Boyuan Yang <byang@debian.org>  Sat, 26 Mar 2022 21:44:37 -0400

3dchess (0.8.1-20) unstable; urgency=medium

  * Team upload.
  * Declare compliance with Debian Policy 4.0.0.
  * Switch to compat level 10.
  * Drop deprecated menu file and xpm icon.
  * Fix FTCBFS.
    - Pass --sourcedirectory to dh.
    - Remove redundant overrides such as override_dh_auto_build thus letting
      dh_auto_build pass cross compilers.
    Thanks to Helmut Grohne for the report and patch. (Closes: #864623)
  * Add wasteful-CPU-consumption.patch. The game always consumed 100 % CPU
    resources due to a missing sleep call in its main loop. (Closes: #866378)

 -- Markus Koschany <apo@debian.org>  Fri, 30 Jun 2017 01:09:34 +0200

3dchess (0.8.1-19) unstable; urgency=medium

  * Team upload.
  * Add 3dchess.xpm icon and replace pieces.xpm since the latter contains
    multiple images and cannot be displayed by Debian's menu system.
    (Closes: #778314)
  * Vcs-Browser: Use https.
  * Declare compliance with Debian Policy 3.9.6.
  * Build without hardening flag stackprotectorstrong because otherwise this
    will make the game unusable.

 -- Markus Koschany <apo@debian.org>  Sun, 01 Nov 2015 00:22:04 +0100

3dchess (0.8.1-18) unstable; urgency=medium

  * Team upload.

  [ Evgeni Golov ]
  * Correct Vcs-* URLs to point to anonscm.debian.org

  [ Jackson Doak ]
  * Rename debian/install to debian/3dchess.install
  * debian/control:
    - Add a homepage
    - Bump debhelper to 9
  * debian/compat: Set as 9
  * debian/rules: Enable hardening

  [ Markus Koschany ]
  * wrap-and-sort -sa.
  * Add new icons. (Closes: #726198)
    - Install 3dchess.png icon to /usr/share/icons/hicolor/256x256/apps.
    - Use pieces.xpm as menu icon and install it to /usr/share/pixmaps/3dchess.
  * Add longtitle and icon entry to menu file.
  * Add keywords, icon and comment field in German to desktop file.
  * Declare compliance with Debian Policy 3.9.5.
  * Add hardening.patch. Support CFLAGS and LDFLAGS.

 -- Markus Koschany <apo@gambaru.de>  Fri, 25 Apr 2014 18:07:21 +0200

3dchess (0.8.1-17) unstable; urgency=low

  * Team upload.
  * Update package description to be more accurate (LP: #602662)
  * Use new quilt format instead of dpatch and merge inc patches
  * Bump Standards-Version, no changes needed
  * Switch to debhelper 7 minimal rules file
  * Fix path to GPLv2 in the copyright file
  * Include a doc-base file pointing at the rules
  * Don't link with libraries that are not used (Xmu Xext)

 -- Paul Wise <pabs@debian.org>  Fri, 18 Feb 2011 18:54:30 +0800

3dchess (0.8.1-16) unstable; urgency=low

  [ Barry deFreese ]
  * Bump Standards Version to 3.8.0. (No changes needed).
  * Update my e-mail address.

  [ Peter De Wachter ]
  * Added watch file.

  [ Christoph Egger ]
  * updating build-depends on replaced packages (Closes: #515353)

 -- Barry deFreese <bdefreese@debian.org>  Mon, 16 Feb 2009 10:22:43 -0500

3dchess (0.8.1-15) unstable; urgency=low

  [ Eddy Petrișor ]
  * fix Vcs-Svn URL to point to svn://, not ssh:// (Closes: #481780)

  [ Barry deFreese ]
  * Build-depend on libxaw7-dev instead of libxaw-headers. (Closes: #484203).
  * Add proper copyright holders to debian/copyright.

 -- Barry deFreese <bddebian@comcast.net>  Mon, 02 Jun 2008 21:38:19 -0400

3dchess (0.8.1-14) unstable; urgency=low

  [ Barry deFreese ]
  * New maintainer (Closes: #363498)
    + Debian Games Team <pkg-games-devel@lists.alioth.debian.org>
  * Move 02-05 patches back in to get out of diff
  * Change VCS tags to Games Team repo
  * Add myself to uploaders
  * Add descriptions for all dpatch files

 -- Barry deFreese <bddebian@comcast.net>  Sat, 29 Dec 2007 18:22:14 -0500

3dchess (0.8.1-13) unstable; urgency=low

  * QA upload.
  * debian/control:
    - Added Vcs-{Svn,Browser} fields to the source stanza, since the package
      has been imported into the "orphaned" branch of collab-maint's
      Subversion repository.
    - Bumped Standards-Version from 3.7.2 to 3.7.3, no changes were required.
  * debian/rules: Do not ignore "make clean" errors, instead test if the
    Makefile is available before calling "$(MAKE) clean".
  * Updated debhelper compatibility level to V5, which is the current
    recommended level and raised the build dependency to "debhelper (>= 5)".
  * Added .desktop file from Salvatore Palma <palma-salvatore@ubuntu.com>
    which has been slightly modified. (closes: #455499)

 -- Frank S. Thomas <fst@debian.org>  Tue, 11 Dec 2007 15:13:23 +0100

3dchess (0.8.1-12) unstable; urgency=low

  * QA Upload
  * Orphaned, set Maintainter to QA Group (#363498)
  * Conforms with new Standards Version
  * Refactor debian/rules
  * Fix menu file (Closes: #341372)
  * Add Build-Dependency on libxaw-headers
  * Remove 3dchessbuild file.
  * Remove typescript file.
  * Add debian/docs
  * Build-Depend on dpatch:
    + split all patches from the .diff.gz into single
      dpatch files.

 -- Michael Ablassmeier <abi@debian.org>  Fri,  5 May 2006 09:43:16 +0200

3dchess (0.8.1-11.1) unstable; urgency=low

  * Non-maintainer upload.
  * xlibs-dev trasition. Closes: #346609

 -- Victor Seva <linuxmaniac@torreviejawireless.org>  Tue, 10 Jan 2006 15:11:00 +0100

3dchess (0.8.1-11) unstable; urgency=low

  * Beef up long description (Closes: #209420)

 --  <bagpuss@debian.org>  Mon, 12 Jan 2004 13:06:33 +0000

3dchess (0.8.1-10testXaw3dg) unstable; urgency=low

  * unreleased, only to test for Xaw3dg changes.

 -- Stephen Stafford <bagpuss@debian.org>  Thu,  7 Aug 2003 14:35:53 +0000

3dchess (0.8.1-10) unstable; urgency=low

  * Ack NMU (Closes: #170144)
  * Bumped policy version to 3.5.9.0

 -- Stephen Stafford <bagpuss@debian.org>  Fri,  9 May 2003 12:11:10 +0000

3dchess (0.8.1-9.1) unstable; urgency=low

  * NMU
  * Removed the obsolote build-dependency on xlib6g-dev.  Closes: #170114.

 -- Daniel Schepler <schepler@debian.org>  Fri, 14 Mar 2003 21:20:12 -0800

3dchess (0.8.1-9) unstable; urgency=low

  * Changed the menu section from Games/Strategy to Games/Board.  This is a
    somewhat grey area, Strategy is not really actually incorrect, but I
    fully agree with the bug submitter that Board is a somewhat more
    intuitive location. (Closes: #128750)
  * tweaked and tidied the rules makefile a little.  Added dh_clean to the
    clean target.
  * Added the section field (6) to the .TH section of the manpage.
    (Closes: 96045)

 -- Stephen Stafford <bagpuss@debian.org>  Sun, 13 Jan 2002 23:08:46 +0000

3dchess (0.8.1-8) unstable; urgency=low

  * New Maintainer (closes: #87154)
  * added simple manpage
  * changed priority to optional (closes: #87480)

 -- Stephen Stafford <bagpuss@debian.org>  Tue, 24 Apr 2001 02:47:54 +0100

3dchess (0.8.1-7) unstable; urgency=low

  * Added Build-Depends (closes: #70057).
  * Recompile (xpm4g is provided by xlibs; closes: #67979).
  * Set maintainer to Debian QA Group (relates: #87154).

 -- Peter Palfrader <weasel@debian.org>  Fri, 23 Feb 2001 23:14:00 +0000

3dchess (0.8.1-6) unstable; urgency=low

  * Recompiled with latest libraries.
  * Upgraded Standards-Version.

 -- Robert S. Edmonds <stu@novare.net>  Mon, 16 Oct 2000 21:21:46 +0000

3dchess (0.8.1-5) unstable; urgency=low

  * Recompiled with latest libraries.

 -- Robert S. Edmonds <stu@novare.net>  Mon, 11 Sep 2000 16:58:07 -0400

3dchess (0.8.1-4) frozen unstable; urgency=low

  * Recompiled with latest libraries.

 -- Robert S. Edmonds <stu@novare.net>  Mon,  5 Apr 1999 21:34:31 -0400

3dchess (0.8.1-3) frozen unstable; urgency=low

  * 3Dc's manpage has been linked to the undocumented man page.

 -- Robert S. Edmonds <edmonds@freewwweb.com>  Mon, 30 Mar 1998 20:04:40 -0500

3dchess (0.8.1-2) unstable; urgency=low

  * Fixed Depends line in the control file. This should fix quite a few bugs.

 -- Robert S. Edmonds <edmonds@freewwweb.com>  Thu, 12 Mar 1998 07:22:25 -0500

3dchess (0.8.1-1) unstable; urgency=low

  * Initial Release.

 -- Robert S. Edmonds <edmonds@freewwweb.com>  Thu,  5 Mar 1998 20:30:01 -0500

